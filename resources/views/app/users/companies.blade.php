<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Company selection') }}
        </h2>
    </x-slot>
    {{-- {{ $companies }} --}}
    <div class="row">
        <div class="col-sm-8 col-md-6 col-lg-4 offset-sm-2 offset-md-3 offset-lg-4">
            <div class="card">
                <div class="card-header">
                    {{ __("Select a company") }}
                </div>
                <div class="list-group list-group-flush">
                    {{-- <a href="#" class="list-group-item list-group-item-action active">
                        Cras justo odio
                    </a> --}}
                    @foreach ($companies as $company)
                    <form action="{{ route('companies.select') }}" method="POST" id="company-{{ $company->id }}">
                        @csrf
                        <input type="hidden" name="company_id" value="{{ $company->id }}">
                    </form>
                    <button type="submit" form="company-{{ $company->id }}" class="list-group-item list-group-item-action">
                        <p class="mb-1 h5">{{ $company->shortname }}</p>
                        <small>{{ $company->fullname }}</small>                        
                    </button>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</x-app-layout>