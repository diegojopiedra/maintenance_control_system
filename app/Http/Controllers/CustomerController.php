<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('app/users/customers_index', [
            'customers' => Customer::paginate()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('app/users/customers_create', [
            'customer' => new Customer(),
            'isNew' => true,
            'editable' => true
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->update($request, null);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show($customer)
    {
        return view('app/users/customers_create', [
            'customer' => Customer::find($customer),
            'isNew' => false,
            'editable' => false
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit($customer)
    {
        return view('app/users/customers_create', [
            'customer' => Customer::find($customer),
            'isNew' => false,
            'editable' => true
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $customer)
    {
        $customer = isset($customer) ? Customer::find($customer) : new Customer();
        $input = $request->only([
            'shortname', 
            'fullname',
            'legal_id',
            'notification_email'
        ]);

        foreach($input as $key => $value){
            $customer->$key = $value;
        }
        $customer->save();
        return redirect(route('customers.edit', $customer->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy($customer)
    {
        $customer = Customer::find($customer);
        $customer->delete();
        return redirect(route('customers.index'));
    }

    public function list(){
        return view('app/users/customer_select', [
            'customers' => Customer::paginate()
        ]);
    }

    public function select(Request $request){
        // dd($request->input());
        $customer = Customer::find($request->input('customer_id'));

        if(null != $customer){
            $maintenance = session('current_maintenance');
            if(null != $maintenance){
                $maintenance->customer_id = $customer->id;
                // dd($maintenance);
                return redirect(route('maintenances.create'));
            }
        }
        
        return back();
        
    }
}
