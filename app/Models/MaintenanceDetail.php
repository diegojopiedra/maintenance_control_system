<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MaintenanceDetail extends Model
{
    use HasFactory;
    protected $connection = 'users_schemas';

    public function device(){
        return $this->belongsTo('App\Models\Device');
    }
}
