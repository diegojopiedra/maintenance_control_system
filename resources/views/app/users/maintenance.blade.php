<x-app-layout>
    <x-slot name="header">
        <div class="d-flex w-100 justify-content-between">
            <span>
                <h2 class="h4 font-weight-bold">
                    {{ __('Maintenance') }} {{ $maintenance->id ? '#' . $maintenance->id : 'nuevo' }}
                </h2>
            </span>
            @if ($isNew)
            <form action="{{ route('maintenances.cancel') }}" method="POST">
                @csrf
                <button class="btn btn-danger btn-sm" type="submit">
                    <i class="fas fa-times"></i> {{ __("Cancel current maintenance") }}
                </button>
            </form>
            @endif
        </div>
    </x-slot>
    {{-- {{ $maintenance }} --}}
    
    {{-- {{ $maintenance->details }} --}}

    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header bg-primary text-white">
                    <i class="fas fa-info-circle"></i> {{ __('Maintenance information') }}
                </div>
                <form action="" method="POST" id="save">
                    @csrf
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <b>{{ __("Customer") }}:</b> 
                            <div>
                                <p class="mb-1 h5 text-primary">{{ $maintenance->customer->shortname }}</p>
                                <small class="text-muted">{{ $maintenance->customer->fullname }}</small>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <b>{{ __("Customer legal id") }}:</b> {{ $maintenance->customer->legal_id }}
                        </li>
                        @if (!$isNew)
                        <li class="list-group-item">
                            <b>{{ __("Date") }}:</b> {{ date('d/m/Y', strtotime($maintenance->date)) }}
                        </li>
                        <li class="list-group-item">
                            <b>{{ __("Hour") }}:</b> {{ date('h:i a', strtotime($maintenance->date)) }}
                        </li>
                        @endif
                        <li class="list-group-item">
                            @if ($editable)
                                <x-input :options="['label' => __('Responsible Technician'), 'name' => 'tecnical', 'required' => true]"/>
                            @else
                                <b>{{ __("Responsible Technician") }}:</b> {{ $maintenance->tecnical }}
                            @endif
                        </li>
                        <li class="list-group-item">
                            @if ($editable)
                                <x-input :options="['label' => __('Customer employee'), 'name' => 'customer_employee', 'required' => true]"/>
                            @else
                                <b>{{ __("Customer employee") }}:</b> {{ $maintenance->customer_employee }}
                            @endif
                        </li>
                        <li class="list-group-item">
                            @if ($editable)
                                <x-input :options="['label' => __('Mail for report'), 'name' => 'report_mail', 'value' => ($maintenance->report_mail?:$maintenance->customer->notification_email), 'required' => true]"/>
                            @else
                                <b>{{ __("Mail for report") }}:</b> {{ $maintenance->report_mail }}
                            @endif
                        </li>
                    </ul>
                </form>
                <div class="card-footer">
                    <button class="btn btn-primary float-right" type="submit" form="save">
                        <i class="fas fa-plus-circle"></i> {{ __("Add details lines") }}
                    </button>
                </div>
            </div>
        </div>
        @if (!$isNew)
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <i class="fas fa-receipt"></i> {{ __("Details lines") }}
                </div>
                <ul class="list-group list-group-flush">
                    @foreach ($maintenance->details as $detail)
                    <li class="list-group-item">
                        <i class="far fa-{{ $detail->execute ? 'check-circle' : 'circle' }}"></i> {{ __("Device") }}:  {{ $detail->device->indentifier }} - {{ $detail->device->description }} {{ $detail->device }}        {{ $detail }}
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endif
    </div>
</x-app-layout>