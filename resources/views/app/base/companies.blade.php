<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Companies') }}
        </h2>
    </x-slot>
    {{ $companies }}
    <a class="btn btn-primary text-white mb-4" href="{{ route('companies.create') }}">
        <i class="fa fa-plus-circle"></i> {{ __("New company") }}
    </a>
    {{-- <div class="card">
        <div class="card-body">
            <a class="btn btn-primary text-white" href="{{ route('companies.create') }}">
                <i class="fa fa-plus-circle"></i> {{ __("New company") }}
            </a>
            @if($companies->count()>0)
            <companies-list
                class="mb-3"
                :data="{{ $companies->map(function ($e) { 
                    $e->show = route('companies.show', $e->id);
                    $e->edit = route('companies.edit', $e->id);
                    $e->destroy = route('companies.destroy', $e->id);
                    return $e;
                }) }}"
            ></companies-list>
            @else
                <p class="text-muted mt-4 display-4">Sin empresas registradas</p>
            @endif
        </div>
    </div> --}}
    <x-data-table
        :paginate="$companies"
        :columns="[
            [
              'name' => __('Legal identification'),
              'key' => 'legal_id'
            ],
            [
              'name' => __('Shortname'),
              'key' => 'shortname'
            ],
            [
              'name' => __('Full name'),
              'key' => 'fullname'
            ],
            [
              'name' => __('Email'),
              'key' => 'notification_email'
            ],
            [
                'name' => 'Acciones',
                'actions' => [
                    [
                        'method' => 'GET',
                        'route' => 'companies.show',
                        'class' => 'btn-sm btn-success text-white',
                        'icon' => 'far fa-eye'
                    ],
                    [
                        'method' => 'GET',
                        'route' => 'companies.edit',
                        'class' => 'btn-sm btn-primary',
                        'icon' => 'far fa-edit'
                    ],
                    [
                        'method' => 'DELETE',
                        'route' => 'companies.destroy',
                        'class' => 'btn-sm btn-danger',
                        'icon' => 'fas fa-trash',
                    ],
                ]
            ],
        ]"
    ></x-data-table>
    {{ $companies }}
</x-app-layout>