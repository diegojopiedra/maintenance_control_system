<?php

namespace App\Http\Controllers;

use App\Models\Connection;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Artisan;

class ConnectionController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $company = Company::find($request->input('company'));
        if(!isset($company)){
            return redirect(route('companies.index'));
        }
        if($company->connection){
            return redirect(route('companies.show', $company));
        }
        return view('app/base/connection', [
            'company' => $company,
            'connection' => new Connection()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->update($request, new Connection());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Connection  $connection
     * @return \Illuminate\Http\Response
     */
    public function edit(Connection $connection)
    {
        $connection->password = Crypt::decryptString($connection->password);
        return view('app/base/connection', [
            'company' => $connection->company,
            'connection' => $connection
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Connection  $connection
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Connection $connection)
    {
        if($request->input('password') == $request->input('password_confirm')){
            $input = $request->only([
                'name', 
                'driver',
                'host',
                'port',
                'database',
                'username',
                'password',
                'company_id'
            ]);
    
            foreach($input as $key => $value){
                
                if($key != 'password'){
                    $connection->$key = $value;
                }else{
                    $connection->password = Crypt::encryptString($value);
                }
            }
            $connection->save();
        }
        if($connection->id){
            return redirect(route('connections.edit', $connection->id));
        }else{
            return back()->withInput();
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Connection  $connection
     * @return \Illuminate\Http\Response
     */
    public function destroy(Connection $connection)
    {
        $company_id = $connection->company_id;
        DB::statement("DROP DATABASE IF EXISTS `{$connection->database}`");
        $connection->delete();
        return redirect(route('companies.edit', $company_id));
    }

    public function generate(Request $request, Company $company){
        if(!isset($company->connection)){
            $connection = new Connection();
            $connection->name = 'Conexión automática a DB de ' . $company->shortname ;
            $connection->driver = env('DEFAULT_DB_DRIVER');
            $connection->host = env('DEFAULT_DB_HOST');
            $connection->port = env('DEFAULT_DB_PORT');
            $connection->database = $this->createDbName($company->shortname);
            $connection->username = env('DEFAULT_DB_USERNAME');
            $connection->password = Crypt::encryptString(env('DEFAULT_DB_PASSWORD'));
            $connection->company_id = $company->id;

            DB::beginTransaction();
            try {
                $this->createDB($connection);
                $this->migrate($connection);

                $connection->save();
                DB::commit();
            } catch (\Throwable $th) {
                DB::rollBack();
            }
            
        }
        return back();
    }

    private function createDbName($company_name) : String{
        $company_name =  Str::lower($company_name);
        $company_name =  Str::of($company_name)->ltrim();
        $company_name = implode('_', explode(' ', $company_name));

        return date_timestamp_get(date_create()) . '-'. $company_name . '-database';
    }

    private function createDB(Connection $connection){
        $schemaName = $connection->database;
        $charset = config("database.connections.mysql.charset",'utf8mb4');
        $collation = config("database.connections.mysql.collation",'utf8mb4_unicode_ci');

        config(["database.connections.mysql.database" => null]);

        $query = "CREATE DATABASE IF NOT EXISTS `$schemaName` CHARACTER SET `$charset` COLLATE `$collation`;";

        DB::statement($query);

        config(["database.connections.mysql.database" => $schemaName]);
    }

    private function migrate(Connection $connection){
        \Config::set("database.connections.users_schemas", [
            "host" => $connection->host,
            "database" => $connection->database,
            "driver" => $connection->driver,
            "username" => $connection->username,
            "password" => Crypt::decryptString($connection->password)
        ]);

        $exitCode = Artisan::queue('migrate', [
            '--path' => 'database\migrations\users_schemas',
            '--database' => 'users_schemas'
        ]);

        // dd(Artisan::output());
    }
}

