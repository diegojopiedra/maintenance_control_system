<?php

namespace App\View\Components;

use Illuminate\View\Component;

class DataTable extends Component
{
    public $paginate;
    public $columns;
    
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($paginate, $columns)
    {
        $this->paginate = $paginate;
        $this->columns = $columns;
        // dd($columns);
    }


    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.data-table');
    }
}
