<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Connection extends Model
{
    use HasFactory;
    protected $hidden = [
        'password'
    ];

    public function company(){
        return $this->belongsTo('App\Models\Company');
    }
}
