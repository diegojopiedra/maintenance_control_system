<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
             
            @isset($customer->shortname)
                {{ __('Customer') }} - {{ $customer->shortname }}
            @else
                 {{ __("New customer") }}
            @endisset
        </h2>
    </x-slot>
    {{-- {{ $customer }} --}}
    @if ($editable)
    @if ($isNew)
    <form action="{{ route('customers.store') }}" method="POST">
    @else
    <form action="{{ route('customers.update', $customer->id) }}" method="POST">
        
        @method('PUT')
    @endif
   
        @csrf
    @endif
        <div class="card">
            <div class="card-header">
                {{ __('Customer') }}
            </div>
            <ul class="list-group list-group-flush">
                
                @if ($editable)
                    <li class="list-group-item">
                        <x-input :options="['label' => __('Short name'), 'name' => 'shortname', 'value' => $customer->shortname]"/>
                    </li>
                    <li class="list-group-item">
                        <x-input :options="['label' => __('Full name'), 'name' => 'fullname', 'value' => $customer->fullname]"/>
                    </li>
                @else
                    <li class="list-group-item">
                        <p class="mb-1 h5 text-primary">{{ $customer->shortname }}</p>
                        <small class="text-muted">{{ $customer->fullname }}</small>
                    </li>
                @endif
                <li class="list-group-item">
                    @if ($editable)
                        <x-input :options="['label' => __('Legal identification'), 'name' => 'legal_id', 'value' => $customer->legal_id]"/>
                    @else
                        <b>{{ __('Legal identification') }}: </b>{{ $customer->legal_id }}
                    @endif
                </li>
                <li class="list-group-item">
                    @if ($editable)
                        <x-input :options="['label' => __('Notification email'), 'name' => 'notification_email', 'value' => $customer->notification_email]"/>
                    @else
                        <b>{{ __('Notification email') }}: </b>{{ $customer->notification_email }}
                    @endif
                </li>
            </ul>
            @if ($editable)
                <div class="card-footer">
                    <button class="btn btn-primary float-right">
                        {{ __("Save") }}
                    </button>
                </div>
            @endif
        </div>
    @if ($editable)
    </form>
    @endif
</x-app-layout>