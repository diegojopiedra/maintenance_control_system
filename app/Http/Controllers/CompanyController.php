<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\User;
use Illuminate\Http\Request; 

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('app/base/companies', [
            'companies' => Company::paginate()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('app/base/company', [
            'company' => new Company(),
            'editable' => true,
            'isNew' => true
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->update($request, new Company());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        return view('app/base/company', [
            'company' => $company,
            'editable' => false,
            'isNew' => false
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('app/base/company', [
            'company' => $company,
            'editable' => true,
            'isNew' => false
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        foreach($request->input() as $key => $value){
            if(!collect(['id', '_token', '_method'])->contains($key)){
                $company->$key = $value;
            }
        }
        $company->save();
        return redirect(route('companies.edit', $company->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        $company->delete();
        return redirect(route('companies.index'));
    }

    public function addUser(Request $request, Company $company){
        try {
            $user = User::where('email', $request->input('email'))->first();
        
            $company->users()->save($user);
            $company->save();
        } catch (\Throwable $th) {
            //throw $th;
        }
        return redirect(route('companies.edit', $company->id));
    }

    public function removeUser(Request $request, Company $company, $user){
        try {
            $company->users()->detach($user);
        } catch (\Throwable $th) {
            //throw $th;
        }
        return redirect(route('companies.edit', $company->id));
    }

    public function myCompanies(Request $request)
    {  
        // dd(auth()->user()->companies);
        return view('app/users/companies', [
            'companies' => auth()->user()->companies
        ]);
    }

    public function selectCompany(Request $request)
    {
        
        $userCompanies = auth()->user()->companies->map(function ($e){
            return $e->id;
        });

        $company_id = $request->input('company_id');

        // dd($userCompanies, $company_id, $userCompanies->contains($company_id));
        
        if($userCompanies->contains($company_id)){
            $company = Company::find($company_id);
            $connection = $company->connection;

            session()->forget('current_maintenance');
            session(['company' => $company]);
            session(['connection' => $connection]);
            return redirect(route('home.index'));
        }

        // dd($request);
        // return 'Se ha registrado el intento de acceso no autorizado por parte de ' . auth()->user()->name . ', con el correo ' . auth()->user()->email . ', desde la IP ' . $request->ip() ;
        return back();
    }
}
