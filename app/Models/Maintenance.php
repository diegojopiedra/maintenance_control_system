<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Maintenance extends Model
{
    use HasFactory;
    protected $connection = 'users_schemas';

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }

    public function details(){
        return $this->hasMany('App\Models\MaintenanceDetail');
    }
}
