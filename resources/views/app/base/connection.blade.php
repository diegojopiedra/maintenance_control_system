<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Connection for') }} {{ $company->shortname }}
        </h2>
    </x-slot>
    <div class="row">
        <div class="col-sm-10 col-md-8 col-lg-6 offset-sm-1 offset-md-2 offset-lg-3">
            @php
                $action = isset($connection->id) ? 
                route("connections.update", $connection->id) : 
                route("connections.store");
            @endphp
            <div class="card">
                <div class="card-header">
                    <a href="{{ route('companies.edit', $company->id) }}" class="btn btn-outline-secondary">
                        <i class="fas fa-chevron-circle-left"></i> {{ __("Go back") }}
                    </a>
                </div>
                <div class="card-body">
                    <form action="{{ $action }}" method="POST" id="main">
                        @csrf
                        @if($connection->id)
                            @method("PUT")
                        @endif
                        @if ($connection->company)
                            <input type="hidden" name="company_id" value="{{ $connection->company->id }}">
                        @else
                            <input type="hidden" name="company_id" value="{{ app('request')->input('company') }}">
                        @endif
                        <p class="h3 mb-1">{{ __("Connection data") }}</p>
                        <x-input :options="['label' => __('Connection name'), 'name' => 'name', 'value' => $connection->name]"></x-input>
                        <x-input :options="['label' => __('Driver'), 'name' => 'driver', 'value' => $connection->driver]"></x-input>
                        <x-input :options="['label' => __('Host'), 'name' => 'host', 'value' => $connection->host]"></x-input>
                        <x-input :options="['label' => __('Port'), 'name' => 'port', 'value' => $connection->port]"></x-input>
                        <x-input :options="['label' => __('Database'), 'name' => 'database', 'value' => $connection->database]"></x-input>
                        <x-input :options="['label' => __('Username'), 'name' => 'username', 'value' => $connection->username ]"></x-input>
                        <x-input :options="['label' => __('Password'), 'name' => 'password', 'value' => $connection->password, 'type' => 'password' ]"></x-input>
                        <x-input :options="['label' => __('Confirm Password'), 'name' => 'password_confirm', 'value' => $connection->password, 'type' => 'password' ]"></x-input>
                    
                    </form>    
                    <form action="#" id="test"></form> 
                </div>
                <div class="card-footer">
                    <div class="float-right">
                        <button class="btn btn-outline-primary" type="submit" form="test">
                            <i class="fas fa-check-circle"></i> {{ __("Test connection") }}
                        </button>
                        <button class="btn btn-primary" type="submit" form="main">
                            <i class="fas fa-save"></i> {{ __("Save") }}
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>