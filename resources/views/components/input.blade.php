<div class="mb-2">
    <div class="form-group">
        <label>{{ $label }}</label>
        <input 
            type="{{ $type }}"
            class="form-control"
            placeholder="{{ $label }}"
            name="{{ $name }}"
            autocomplete="off"
            value="{{ $value }}"
        >
    </div>
</div>