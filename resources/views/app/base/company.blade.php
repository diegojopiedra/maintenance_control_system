<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Company') }}
        </h2>
    </x-slot>
    <div class="row">
        <div class="col-md-6 col-lg-8 mb-4">
            @if($editable)
                <form method="POST" action="{{ $isNew ? route('companies.store') : route('companies.update', $company->id)}}">
                    @csrf
                    @if(!$isNew)
                        @method("PUT")
                    @endif
            @endif
            <div class="card">
                <div class="card-header bg-secondary text-white">
                    <i class="fa fa-info-circle"></i> {{ __("Company information") }}
                </div>
                
                @if($editable)
                <div class="card-body">
                    <x-input :options="['label' => __('Short name'), 'name' => 'shortname', 'value' => $company->shortname]"/>
                    <x-input :options="['label' => __('Full name'), 'name' => 'fullname', 'value' => $company->fullname]"/>
                    <x-input :options="['label' => __('Legal identification'), 'name' => 'legal_id', 'value' => $company->legal_id]"/>
                    <x-input :options="['label' => __('Notification email'), 'name' => 'notification_email', 'value' => $company->notification_email]"/>
                </div>
                @else
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                            <h5 class="card-title">{{ $company->shortname }}</h5>
                            <p class="mb-1">{{ $company->fullname }}</p>
                    </li>
                    <li class="list-group-item">
                        <b>{{ __('Legal identification') }}:</b> {{ $company->legal_id }}
                    </li>
                    <li class="list-group-item">
                        <b>{{ __('Notification email') }}:</b> {{ $company->notification_email }}
                    </li>
                </ul>
                @endif
                @if($editable)
                    <div class="card-footer">
                        <button class="btn btn-primary float-right">
                            <i class="fa fa-save"></i> {{ __("Save") }}
                        </button>
                    </div>
                @endif
            </div>
            @if($editable)
                </form>
            @endif
        </div>
        <div class="col-md-6 col-lg-4">
            @if(!$isNew)
            <div class="card mb-4">
                <div class="card-header bg-secondary text-white">
                    <i class="fa fa-users"></i> {{ __("Users related with") }} {{ $company->shortname }}
                </div>
                <div class="list-group list-group-flush">
                    @foreach ($company->users as $user)
                    <div class="list-group-item">
                        @if($editable)
                        <form action="{{ route('companies.removeUser', [$company->id, $user->id]) }}" method="POST">
                            @csrf
                            @method("DELETE")
                            <button class="btn btn-outline-secondary btn-sm float-right">
                                <i class="fa fa-trash"></i>
                            </button>
                        </form>
                            
                        @endif
                        <p class="mb-0">{{ $user->name }}</p>
                        <small class="text-muted">{{ $user->email }}</small>
                    </div>
                    @endforeach
                    @if($editable)
                    <div class="list-group-item">
                        <form action="{{ route('companies.addUser', $company->id) }}" method="POST">
                            @csrf
                            <div class="input-group mb-0">
                                <input type="email" class="form-control" placeholder="{{ __("Email") }}" required name="email">
                                <div class="input-group-append">
                                  <button class="btn btn-primary text-white" type="submit"><i class="fa fa-plus-circle"></i> {{ __("Add") }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    @else
                        @if($company->users->count()<1)
                        <div class="list-group-item">
                            <p class="text-muted mb-0"><i class="fa fa-ban"></i> Sin usuarios asignados</p>
                        </div>
                        @endif
                    @endif
                </div>
            </div>
            @endif
            @if(!$isNew)
            <div class="card">
                <div class="card-header bg-secondary text-white">
                    <i class="fa fa-database"></i> {{ __("Data base schema") }}
                </div>
                @if ($company->connection)
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <b>{{ __("Connection name") }}</b>: {{ $company->connection->name }}
                        </li>
                        <li class="list-group-item">
                            <b>{{ __("Driver") }}</b>: {{ $company->connection->driver }}
                        </li>
                        <li class="list-group-item">
                            <b>{{ __("Host") }}</b>: {{ $company->connection->host }}
                        </li>
                        <li class="list-group-item">
                            <b>{{ __("Port") }}</b>: {{ $company->connection->port }}
                        </li>
                        <li class="list-group-item">
                            <b>{{ __("Database") }}</b>: {{ $company->connection->database }}
                        </li>
                        <li class="list-group-item">
                            <b>{{ __("Username") }}</b>: {{ $company->connection->username }}
                        </li>
                    </ul>
                @else
                <div class="card-body">
                    <p class="h3 text-muted">{{ __('Schema not exist') }}</p>
                    @if($editable)
                    <form action="{{ route('connections.generate',  $company->id) }}" method="POST">
                        @csrf
                        <button class="btn btn-primary mb-2">
                            <i class="fas fa-robot"></i> {{ __('Automatically generate new schema') }}
                        </button>
                    </form>
                    
                    <a href="{{ route('connections.create', ['company' => $company->id]) }}" class="text-primary">
                        <i class="far fa-edit"></i> {{ __('Connect to existing schema') }}
                    </a>
                    @endif
                </div>
                @endif
                @if($editable && !$isNew && $company->connection)
                <div class="card-footer">
                    <div class=" float-right">
                        <a class="btn btn-primary" href="{{ route("connections.edit", $company->connection->id) }}">
                            <i class="fa fa-edit"></i> {{ __("Edit") }}
                        </a>
                        <form action="{{ route('connections.destroy', $company->connection->id) }}" method="POST" style="display: inline-block">
                            @csrf
                            @method("DELETE")
                            <button class="btn btn-danger">
                                <i class="fa fa-trash"></i> {{ __("Delete") }}
                            </button> 
                        </form>
                    </div>
                </div>
                @endif
            </div>
            @endif
        </div>
    </div>
</x-app-layout>