<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\ConnectionController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MaintenanceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// dd(session('connection'));

// $connection = session('connection');
// \Config::set("database.connections.users_schemas", [
//     "host" => $connection->host,
//     "database" => $connection->database,
//     "driver" => $connection->driver,
//     "username" => $connection->username,
//     "password" => Crypt::decryptString($connection->password)
// ]);

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    // Route::get('/dashboard', function () {
    //     return view('dashboard');
    // })->name('dashboard');

    Route::post('/companies/{company}/users', [CompanyController::class, 'addUser'])->name('companies.addUser');
    Route::delete('/companies/{company}/users/{user}', [CompanyController::class, 'removeUser'])->name('companies.removeUser');
    Route::get('/companies/select', [CompanyController::class, 'myCompanies'])->name('companies.myCompanies');
    Route::post('/companies/select', [CompanyController::class, 'selectCompany'])->name('companies.select');
    Route::resource('/companies', CompanyController::class);

    Route::resource('/connections', ConnectionController::class)->except(['index', 'show']);
    Route::post('/connections/generate/{company}', [ConnectionController::class, 'generate'])->name('connections.generate');


    Route::middleware('conn')->group(function () {
        Route::get('/home', [HomeController::class, 'index'])->name('home.index');
        Route::post('/maintenances/cancel', function (){
            session()->forget('current_maintenance');
            return redirect(route('home.index'));
        })->name('maintenances.cancel');
        Route::resource('/maintenances', MaintenanceController::class);
        Route::get('/customers/select', [CustomerController::class, 'list'])->name('customers.list');
        Route::post('/customers/select', [CustomerController::class, 'select'])->name('customers.select');
        
        Route::resource('/customers', CustomerController::class);

    });
    
});


// Route::middleware('conn')->group(function () {
//     Route::resource('/maintenances', MaintenanceController::class);
// });


