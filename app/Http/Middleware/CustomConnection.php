<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class CustomConnection
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(session()->has('connection')){
            $connection = session('connection');
            \Config::set("database.connections.users_schemas", [
                "host" => $connection->host,
                "database" => $connection->database,
                "driver" => $connection->driver,
                "username" => $connection->username,
                "password" => Crypt::decryptString($connection->password)
            ]);   
        }else{
            return redirect(route('companies.select'));
        }
        return $next($request);
    }
}
