<div>
    <div class="card shadow">
        <div class="card-header bg-primary">
            <form action="">
                @csrf
                <div class="row">
                    <div class="col-2 g-2">
                        <div class="input-group">
                            <span class="input-group-text">
                                <i class="fas fa-list-ol"></i>
                            </span>
                            <select class="form-select" name="limit">
                                <option value="5">5 {{ __("lines") }}</option>
                                <option value="10">10 {{ __("lines") }}</option>
                                <option value="25">25 {{ __("lines") }}</option>
                                <option value="50">50 {{ __("lines") }}</option>
                                <option value="100">100 {{ __("lines") }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-4 g-2">
                        <div class="input-group">
                            <span class="input-group-text">
                                <i class="far fa-eye"></i>
                            </span>
                            <select class="form-select" name="column">
                                <option>Columana A</option>
                                <option>Columana B</option>
                                <option>Columana C</option>
                                <option>Columana D</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-6 g-2">
                        <div class="input-group">
                            <span class="input-group-text">
                                <i class="fa fa-search"></i>
                            </span>
                            <input type="text" class="form-control" placeholder="{{ __('Search') }}" name="search">
                            <button type="button" class="btn btn-dark">{{ __('Search') }}</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <table class="table mb-0 table-striped table-hover table-borderless">
            <thead>
              <tr>
                    @foreach ($columns as $column)
                        <th>
                            {{ $column['name'] }}
                        </th>
                    @endforeach
              </tr>
            </thead>
            <tbody>
                @foreach ($paginate as $itemIndex => $item)
                    <tr>
                        @foreach ($columns as $colIndex => $column)
                        
                            <td>
                                @if (isset($column['key']))
                                    @php
                                        $key = $column['key'];
                                    @endphp
                                    {{ $item->$key }}
                                @else
                                    @if (isset($column['slot']))
                                        @php
                                            $name = $column['slot'];
                                        @endphp
                                        {{ $$name }}
                                    @else
                                        @if (isset($column['actions']))
                                            @foreach ($column['actions'] as $index => $action)
                                                <form style="display: none" id="action-{{ $itemIndex }}-{{ $colIndex }}-{{ $index }}" action="{{ route($action['route'], $item->id) }}" method="POST">
                                                    @csrf
                                                    @method($action['method'])
                                                </form>
                                                <button
                                                    class="btn {{ isset($action['class'])?$action['class']:'' }}"
                                                    type="submit"
                                                    form="action-{{ $itemIndex }}-{{ $colIndex }}-{{ $index }}"
                                                >
                                                    @isset($action['icon'])
                                                        <i class="{{ $action['icon'] }}"></i>
                                                    @endisset
                                                    @isset($action['text'])
                                                        {{ $action['text'] }}
                                                    @endisset
                                                </button>
                                            @endforeach
                                        @endif
                                    @endif
                                @endif
                            </td>
                           
                        @endforeach
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="card-footer">
            {{ $paginate->links()  }} {{ $slot }}
        </div>
    </div>
</div>