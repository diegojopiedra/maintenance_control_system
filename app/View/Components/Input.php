<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Input extends Component
{
    public $label;
    public $name;
    public $value;
    public $type;
    public $required;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($options = [])
    {
        extract($options);
        $this->label = isset($label)?$label:"";
        $this->name = isset($name)?$name:"";
        $this->value = isset($value)?$value:"";
        $this->type = isset($type)?$type:"";
        $this->required = isset($required)?$required:"";
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.input');
    }
}
