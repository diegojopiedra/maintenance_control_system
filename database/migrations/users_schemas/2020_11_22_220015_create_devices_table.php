<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->id();
            $table->string('indentifier');
            $table->foreignId('site_id')->constrained()->onDelete('cascade'); 
            // $table->foreignId('device_type_id')->constrained('device_types')->nullable()->onDelete('SET NULL'); 
            $table->unsignedBigInteger('device_type_id')->nullable()->onDelete('SET NULL'); 
            $table->foreign('device_type_id')->references('id')->on('device_types');
            $table->string('description');
            $table->date('next_maintenance');
            $table->date('next_consumable_change');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
}
