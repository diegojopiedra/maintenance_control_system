<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Customers') }}
        </h2>
    </x-slot>
    <div class="row">
        <div class="col-sm-8 col-md-6 col-lg-4 offset-sm-2 offset-md-3 offset-lg-4">
            <div class="card">
                <div class="card-header">
                    {{ __("Select customer") }}
                </div>
                <div class="list-group list-group-flush">
                    @foreach ($customers as $customer)
                    <form action="{{ route('customers.select') }}" method="POST" id="customer-{{ $customer->id }}">
                        @csrf
                        <input type="hidden" name="customer_id" value="{{ $customer->id }}">
                    </form>
                    <button type="submit" form="customer-{{ $customer->id }}" href="#" class="list-group-item list-group-item-action">
                        <p class="mb-1 h5">{{ $customer->shortname }}</p>
                        <small>{{ $customer->fullname }}</small>                        
                    </button>
                    @endforeach
                </div>
                <div class="card-footer">
                    <a href="" class="btn btn-primary float-right">
                        <i class="fas fa-plus-circle"></i> {{ __("Add new customer") }}
                    </a>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>