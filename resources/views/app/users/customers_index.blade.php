<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Customers') }}
        </h2>
    </x-slot>
    <a href="{{ route('customers.create') }}" class="btn btn-primary mb-4">
        <i class="fa fa-plus-circle"></i> {{ __("Add customer") }}
    </a>
    <x-data-table
        :paginate="$customers"
        :columns="[
            [
              'name' => 'Empresa',
              'key' => 'shortname'
            ],
            [
              'name' => 'Cédula juridica',
              'key' => 'legal_id'
            ],
            [
              'name' => 'Correo para notificación',
              'key' => 'notification_email'
            ],
            [
                'name' => 'Acciones',
                'actions' => [
                    [
                        'method' => 'GET',
                        'route' => 'customers.show',
                        'class' => 'btn-sm btn-success text-white',
                        'icon' => 'far fa-eye'
                    ],
                    [
                        'method' => 'GET',
                        'route' => 'customers.edit',
                        'class' => 'btn-sm btn-primary',
                        'icon' => 'far fa-edit'
                    ],
                    [
                        'method' => 'DELETE',
                        'route' => 'customers.destroy',
                        'class' => 'btn-sm btn-danger',
                        'icon' => 'fas fa-trash',
                    ],
                ]
            ],
        ]"
    ></x-data-table>
</x-app-layout>