<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Dashboard') }} - {{ session('company')->shortname }}
        </h2>
    </x-slot>
    {{-- {{ $maintenances }} - {{ $customers }} --}}
    <div class="row mb-3">
        <div class="col-sm-6 col-md-8">
            <div class="d-flex w-100 justify-content-between">
                <h1 class="mb-1">
                    <i class="fas fa-wrench"></i> {{ __("Maintenances") }}
                </h1>
                <a href="{{ route("maintenances.create") }}" class="btn btn-primary mb-3">
                    <i class="fas fa-plus-circle"></i> {{ __("New maintenance") }}
                </a>
              </div>
        </div>
    </div>
    {{ $maintenances }}
    <div class="row">
        <div class="col-sm-6 col-md-8">
            @foreach ($maintenances as $maintenance)
            <div class="card mb-4 shadow">
                <div class="card-header">
                    <div class="d-flex w-100 justify-content-between">
                        <div>
                            
                            <p class="mb-1 h5">
                                <span class="badge bg-primary mb-1">
                                    {{ __('Maintenance') }}  #{{ $maintenance->id }}
                                </span>
                            </p>
                            <p class="mb-1 h5 text-primary">{{ $maintenance->customer->shortname }}</p>
                            <small class="text-muted">{{ $maintenance->customer->fullname }}</small>
                        </div>
                        <div>
                            <span class="badge bg-dark mb-1">
                                <i class="fas fa-calendar-alt"></i> {{ date('d/m/Y', strtotime($maintenance->date)) }}
                            </span>
                            <p class="text-muted small mb-1">
                                <i class="fas fa-clock"></i> {{ date('h:i a', strtotime($maintenance->date)) }}
                            </p>
                        </div>
                    </div>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <b>{{ __("Details lines") }}:</b> 5
                    </li>
                    <li class="list-group-item">
                        <b>{{ __("Technician") }}:</b> {{ $maintenance->tecnical }}
                    </li>
                    <li class="list-group-item">
                        <b>{{ __("Empleado del cliente ") }}:</b> {{ $maintenance->customer_employee }}
                    </li>
                    <li class="list-group-item">
                        <b>{{ __("Report email") }}:</b> {{ $maintenance->report_mail }}
                    </li>
                </ul>
                <div class="card-footer">
                    <a href="{{ route("maintenances.show", $maintenance->id) }}" class="btn btn-primary float-right">
                        <i class="fas fa-info-circle"></i> {{ __("See details") }}
                    </a>
                </div>
            </div>
            @endforeach
            
            @if ($maintenances->total() < 1)
            <div class="card">
                <div class="card-body">
                    <div class="jumbotron">
                        <p class="display-4 mb-1">
                            {{ __("Welcome") }}
                        </p>
                        <p class="lead">
                            {{ __("In this area appears a list of all the maintenance that the company has carried out") }}
                        </p>
                        {{-- <hr class="my-4">
                        <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
                        <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a> --}}
                    </div>
                </div>
            </div>
            
            @endif
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="card">
                <div class="card-header">
                    <button class="btn btn-secondary btn-sm float-right">
                        {{ __("Manage clients") }}
                    </button>
                    {{ __("My clientes") }}
                </div>
                <div class="list-group list-group-flush">
                    <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                      <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">List group item heading</h5>
                        <small>3 days ago</small>
                      </div>
                      <p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
                      <small>Donec id elit non mi porta.</small>
                    </a>
                </div>
            </div>
        </div>
    </div>
    {{ $maintenances }}
</x-app-layout>