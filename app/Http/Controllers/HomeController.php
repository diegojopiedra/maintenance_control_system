<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Maintenance;
use App\Models\Customer;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        return view('app/users/home', [
            'maintenances' => Maintenance::orderBy('id', 'desc')->paginate(2),
            'customers' => Customer::all()->take(5)
        ]);
    }
}
